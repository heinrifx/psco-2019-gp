#include <limits>
#include <queue>
#include <cassert>
#include <algorithm>

#include <gp-bnb/edmonds_karp.hpp>

edmonds_karp::edmonds_karp(const graph &g, std::vector<node_id> sources, std::vector<node_id> sinks) 
    : g_(g), sources_(sources), sinks_(sinks) {
	
	index_edges();
	int num_edges = indexed_edges_.size() / 2;

	// Initialize vectors
	flow_.assign(num_edges, 0);
	sources_and_sinks_.assign(g_.num_nodes() + 1, 0);
	resid_flow_.assign(num_edges, 0);

	// Sources marked as 1, sinks marked as -1. All other nodes are 0.
    for (auto s : sources_)
        sources_and_sinks_[s] = 1;
    for (auto t : sinks_) 
        sources_and_sinks_[t] = -1;
};

/* Indexes edges of the graph
Every edge in the graph is mapped to its unique id
indexed_edges std::map consists of key: pair<node_id, node_id> value: edge_id */
void edmonds_karp::index_edges() {

	unsigned int num_nodes = g_.num_nodes();
	unsigned int eid = 0;

	for (node_id u = 1; u <= num_nodes; u++) {

		std::vector<node_id> neighbors = g_.get_adjacency(u);

		for (node_id v : neighbors) {
			auto node_pair = std::make_pair(u, v);
			
			// edge was already listed
			if (indexed_edges_.count(std::make_pair(v, u)) == 1) {
				indexed_edges_[node_pair] = indexed_edges_.at(std::make_pair(v, u));
			
			} else {
				indexed_edges_[node_pair] =  eid;
				eid++;
			}
		}
	}
}

/* Breadth-first search in the graph from source nodes to sink nodes */
std::pair<int, node_id> edmonds_karp::bfs(std::vector<unsigned int> &pred) const {

	pred.clear();
	pred.resize(g_.num_nodes() + 1, -1); // undiscovered nodes are marked with -1
	std::vector<int> gain(g_.num_nodes() + 1, 0);
	bool sink_reached = false;
	node_id sink;

	// Push all source nodes to the queue
	std::queue<node_id> q;
	for (node_id s : sources_) {
		q.push(s);
		pred[s] = s;
		gain[s] = std::numeric_limits<int>::max();
	}

	// Perform BFS from each source node and stop when the sink was reached
	// or there was no path found from the source to the sink
	while (!q.empty() && !sink_reached) {
		node_id u = q.front();
		q.pop();

		std::vector<node_id> neighbors = g_.get_adjacency(u);

		// iterate through neighbors of u
		for (node_id v : neighbors) {
			unsigned int edge_id = indexed_edges_.at(std::make_pair(u, v));
			int edge_weight = 1; // unweighted graph

			if (((u >= v && flow_[edge_id] < edge_weight) 
			|| ( u < v && resid_flow_[edge_id] < edge_weight))
			&& pred[v] == (unsigned int) -1) { // only add those neighbors with rest capacity and which were not discovered yet
				pred[v] = u;
				gain[v] = std::min(gain[u], edge_weight - (u >= v ? flow_[edge_id] : resid_flow_[edge_id]));
	
				if (!sink_reached) {
					q.push(v);
					sink = v;
				}
				// Check if any of the sink nodes are reached
				if (sources_and_sinks_[v] == -1)
					sink_reached = true;
			}
		}

		if (sink_reached)
			return std::make_pair(gain[sink], sink);

	}

	return std::make_pair(0.0, sink);
};


/* Edmonds-Karp Algorithm for unweighted, undirected graphs
Step 0: Index graph edges 
Step 1: Initialize flow and residual flow vectors with length |E|
--Loop:
Step 2: Perform BFS that returns max gain from source to sink in one path
Step 3: Add gain that was calculated during BFS to the flow value. Break from the loop if gain was 0
Step 4: Update flow and residual flow values for each node */
void edmonds_karp::run() {

	flow_value_ = 0;

	while (true) {
		std::vector<node_id> pred;

		// BFS from source nodes to sink nodes
		auto bfs_result = bfs(pred);
		int gain = bfs_result.first;
		node_id v = bfs_result.second;

		if (gain == 0) break;

		// Add gain that was calculated during BFS to the flow value
		flow_value_ += gain;

		// Update flow and residual flow values for each edge 
		// in the path from the sink node to the source node
		bool source_reached = false;
		while (!source_reached) {
			node_id u = pred[v];
			int edge_id = indexed_edges_[std::make_pair(u, v)];

			if (u >= v) {
				flow_[edge_id] += gain;
				resid_flow_[edge_id] -= gain;
			} else {
				flow_[edge_id] -= gain;
				resid_flow_[edge_id] += gain;
			}

			v = u;
			// Check if any of the sink nodes are reached
			if (sources_and_sinks_[v] == 1) 
				source_reached = true;
		}
	}
	
};

int edmonds_karp::get_max_flow() const {
    return flow_value_;
};
