#include <gp-bnb/graph.hpp>

graph::graph(std::vector<std::vector<unsigned int>> a) : nodes_(a.size()), adjacency_list_(a) {
}

unsigned int graph::num_nodes() const {
    return nodes_;
}

std::vector<unsigned int> graph::get_adjacency(node_id v) const {
    return adjacency_list_[v-1];
}
