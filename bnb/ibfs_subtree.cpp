#include <gp-bnb/ibfs_subtree.hpp>

ibfs_subtree::ibfs_subtree(std::vector<node_id> roots, int id, const graph& g) : id_(id), g_(g), labels_(std::vector<std::unordered_set<node_id>> {std::unordered_set<node_id> (roots.begin(), roots.end()), std::unordered_set<node_id> () }) {
};

void ibfs_subtree::increment_current_max() {
    labels_.push_back(std::unordered_set<node_id> ());
    current_max_++;
};

void ibfs_subtree::add_node(label l, node_id node, node_id pred) {
    labels_[l].insert(node);
    succ_[pred].insert(node);
    pred_[node] = pred;
};

std::vector<node_id> ibfs_subtree::reduce_path(node_id leaf) {
    last_flow_.clear();
    last_resid_orphans_.clear();

    std::vector<node_id> new_front_nodes;

    node_id current = leaf;
    // deletes the nodes of the augmentation path and adopts orphans
    for (label i = current_max_; i > 0; --i) {
        last_flow_.push_back(current);
        labels_[i].erase(current);
       
        // searches and adopts orphans iteratively
        std::vector<std::unordered_set<node_id>> orphans;
        orphans.push_back(succ_[current]);
        for (label j = i + 1; j <= current_max_; ++j) {
            std::unordered_set<node_id> next_back;
            for (node_id n : orphans.back()) {
                for (node_id m : succ_[n]) {
                    next_back.insert(m);
                }
                labels_[j].erase(n);
                pred_.erase(n);
                succ_.erase(n);
            }
            if (!next_back.empty()) {
                orphans.push_back(next_back);
            }
        }
        for (label j = 0; j < orphans.size(); ++j) {
            for (node_id n : orphans[j]) {
                label l = adopt(n, i+j);
                if (l == 0) {
                    last_resid_orphans_.push_back(n);
                } else if (l == current_max_) {
                    new_front_nodes.push_back(n);
                }
            }
        }
        
        node_id pred = pred_[current];
        succ_[pred].erase(current);

        pred_.erase(current);
        succ_.erase(current);

        current = pred;
    }
    return new_front_nodes;
};

// tries to insert orphans with minimum label
// if they could have been inserted with label <= min,
// they would have happened because of BFS
label ibfs_subtree::adopt(node_id orphan, label min) {
    for (label i = min; i < current_max_; ++i) {
        for (node_id a : labels_[i]) {
            for (node_id n : g_.get_adjacency(a)) {
                if (n == orphan) {
                    add_node(i+1, orphan, a);
                    return i+1;
                }
            }
        }
    }
    return 0;
};

