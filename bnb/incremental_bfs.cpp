#include <cassert>
#include <unordered_set>
#include <queue>

#include <gp-bnb/incremental_bfs.hpp>

incremental_bfs::incremental_bfs(const graph& g, std::vector<node_id> sources, std::vector<node_id> sinks) 
    : g_(g), sources_(sources), sinks_(sinks), s_(ibfs_subtree(sources, subtree::s, g)), t_(ibfs_subtree(sinks, subtree::t, g)) {
    assert(!sources.empty());
    assert(!sinks.empty());
    assert(sources.size() <= g.num_nodes()/2);
    assert(sinks.size() <= g.num_nodes()/2); 

    node_assignments_ = std::vector<subtree>(g.num_nodes(), none);
    for (node_id node : sources) {
        node_assignments_[node-1] = s_root;
        
    }
    for (node_id node : sinks) {
        node_assignments_[node-1] = t_root;
    }
};

void incremental_bfs::run() {
    // increments the flow value to the number of pairwise neighbors of sources and sinks
    for (node_id node : s_.get_front()) {
        for (node_id neighbor : g_.get_adjacency(node)) {
            if (node_assignments_[neighbor-1] == t_root) {
                flow_++;
            }
        } 
    }
    
    // grows the subtrees alternating until one subtree cannot grow anymore
    for (int i = 0; ; ++i) {
        if (i%2 == 0) {
            if (!grow(s_)) break;
        } else {
            if (!grow(t_)) break;
        }
    }
};

bool incremental_bfs::grow(ibfs_subtree& st) {
    // creates a queue of candidate nodes for extending
    std::queue<node_id> q;
    for (node_id n : st.get_front()) {
        q.push(n);
    }

    while (!q.empty()) {
        node_id node = q.front();
        q.pop();
        // searches for neighbors of candidate nodes
        for (node_id neighbor : g_.get_adjacency(node)) {
            // performs augmentation ...
            if (-st.get_id() == node_assignments_[neighbor-1]) {
                if (st.get_id() == subtree::s) {
                    for (node_id n : s_.reduce_path(node)) {
                        q.push(n);
                    }
                    t_.reduce_path(neighbor);
                    
                } else {
                    for (node_id n : t_.reduce_path(node)) {
                        q.push(n);
                    }
                    s_.reduce_path(neighbor);
                    
                } 

                for (node_id n : s_.get_last_flow()) {
                    node_assignments_[n-1] = flow;
                }
                for (node_id n : s_.get_last_resid_orphans()) {
                    node_assignments_[n-1] = none;
                }
                for (node_id n : t_.get_last_flow()) {
                    node_assignments_[n-1] = flow;
                }
                for (node_id n : t_.get_last_resid_orphans()) {
                    node_assignments_[n-1] = none;
                }

                flow_++;
                break;
            // ... or extends st
            } else if (node_assignments_[neighbor-1] == none) {
                node_assignments_[neighbor-1] = (st.get_id() == subtree::s) ? s : t;
                st.add_node(st.get_current_max()+1, neighbor, node);
            }
        }
                
    }

    // increments the label's upper bound of st
    st.increment_current_max();

    // determines if the tree is not grown
    if (st.get_front().empty()) {
        return false;
    }
    return true;
};


