#include <cassert>
#include <iostream>
#include <vector>

#include <gp-bnb/bnb.hpp>
#include <gp-bnb/graph.hpp>
#include <gp-bnb/partition.hpp>
#include <gp-bnb/metis_reader.hpp>

int main(int argc, char** argv) {
    assert(argc > 1);

    std::vector<std::string> args(argv, argv+argc);

    std::string graphFile = args[1];
    auto g = metis_reader().read(graphFile);

    std::cerr << "Number of nodes: " << g.num_nodes() << std::endl;

    auto sol = gp_bnb::solver(g, gp_bnb::lb::none);

    std::string current_option;

    for (int i = 2; i < argc; ++i) {
        if (i%2 == 0) {
            current_option = args[i];
        } else {
            if (current_option == "-l") {
                if (args[i] == "ek") { 
                    sol = gp_bnb::solver(g, gp_bnb::lb::ek);
                } else if (args[i] == "ibfs") {
                    sol = gp_bnb::solver(g, gp_bnb::lb::ibfs);
                } else if (args[i] == "pr") {
                    sol = gp_bnb::solver(g, gp_bnb::lb::pr);
                } else if (args[i] == "gp") {
                    sol = gp_bnb::solver(g, gp_bnb::lb::gp);
                }
            }
        }
    }

    sol.solve();

    /*
    int i = 1;
    for(auto g : sol.best_solution()){
        
        std::cerr << i++ << ": " << g << std::endl;

    }
    */

}

