#include <gp-bnb/metis_reader.hpp>

#include <stdexcept>

std::vector<unsigned int> parse_line(std::string& line) {
    std::vector<unsigned int> adjacencies;
    std::stringstream iss( line );
    int node;
    while ( iss >> node ) {
        adjacencies.push_back( node );
    }
	return adjacencies;
}


graph metis_reader::read(std::string& path) {
    
    std::ifstream graph_file(path);
    std::pair<int, int> header;
    std::vector<std::vector<unsigned int>> adjacencies;
    int edge_counter = 0;

    std::string line = "";

    if (std::getline(graph_file, line)) {
		// ignore comment lines
		while (line[0] == '%')
			std::getline(graph_file, line);

        // read the header line
        std::vector<unsigned int> first_line = parse_line(line);
        header = std::make_pair(first_line[0], first_line[1]);
        adjacencies.reserve(header.first);

        std::cout << "Reading graph in Metis format ";
        std::cout << "(n = " << header.first << ", m = " << header.second << ")" << std::endl;

        while (getline(graph_file, line)) {
            // ignore comment lines after the header
            if (line[0] != '%') {
                adjacencies.push_back(parse_line(line));
                edge_counter += adjacencies.back().size();
            }
        }
    }

    if ( edge_counter / 2 != header.second )
        puts("Reader Error: incorrect number of edges");
    if ( adjacencies.size() != (unsigned int) header.first )
        puts("Reader Error: incorrect number of nodes");

    graph g = graph(adjacencies);
    return g;
}