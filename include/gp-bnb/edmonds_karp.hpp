#ifndef EDMONDSKARP_H_
#define EDMONDSKARP_H_

#include <vector>
#include <map>
#include <gp-bnb/graph.hpp>

class edmonds_karp {
private:
	const graph &g_;

	std::map<std::pair<node_id, node_id>, unsigned int> indexed_edges_;

	std::vector<node_id> sources_;
	std::vector<node_id> sinks_;
	std::vector<int> sources_and_sinks_;

	int flow_value_;
	std::vector<int> flow_;
	std::vector<int> resid_flow_;


	/**
	 * Indexes edges of the graph
	 * Every edge in the graph is mapped to its unique id
	 */
	void index_edges();
	/**
	 * Performs a breadth-first search on the graph from the source node to find an augmenting path to the sink node respecting the flow values
	 * @param residFlow The residual flow in the network.
	 * @param pred Used to store the path from the source to the sink.
	 * @return The gain in terms of flow and id of the sink node.
	 */
	std::pair<int, node_id> bfs(std::vector<unsigned int> &pred) const;

public:
	/**
	 * Constructs an instance of the EdmondsKarp algorithm for the given graph, source and sink
	 * @param graph The graph.
	 * @param source The source node.
	 * @param sink The sink node.
	 */
	 edmonds_karp(const graph &g, std::vector<node_id> sources, std::vector<node_id> sinks);

	/**
	 * Computes the maximum flow, executes the EdmondsKarp algorithm.
	 * For unweighted, undirected Graphs.
	 */
	void run();

	/**
	 * Returns the value of the maximum flow from source to sink.
	 *
	 * @return The maximum flow value
	 */
	int get_max_flow() const;
};

#endif /* EDMONDSKARP_H_ */
