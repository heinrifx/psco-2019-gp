#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <vector>

using node_id = unsigned int; 

// Represents the input graph to be partitioned
class graph {
public:    

    /** @brief  Initializes the graph to be partitioned
     *  @param  Adjacency lists
     */
    graph(std::vector<std::vector<unsigned int>> a);

    /** @brief  Returns the number of nodes of the graph
     *  @return Number of nodes
     */
    unsigned int num_nodes() const;
    
    /** @brief  Provides access to the adjacency of a node  
     *  @param  Node Id
     *  @return Adjacency of the node
     */
    std::vector<unsigned int> get_adjacency(node_id v) const;

private:
    unsigned int nodes_;
	std::vector<std::vector<unsigned int>> adjacency_list_;
};

#endif
