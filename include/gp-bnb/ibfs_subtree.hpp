#ifndef IBFSSUBTREE_HPP_
#define IBFSSUBTREE_HPP_

#include <map>
#include <unordered_set>
#include <vector>
#include "graph.hpp"

using label = unsigned int; 
using node_id = unsigned int;

class ibfs_subtree {
public:    

    /** @brief  Initializes a subtree for incremental bfs
     *  @param  List of roots (sources or sinks)
     */
    ibfs_subtree(std::vector<node_id> roots, int id, const graph& g);
    
    /** @brief  Deletes the path of the new flow of this subtree 
     *          Part of the augmentation of this subtree
     *  @param  Leaf with edge to the leaf of the other tree along the augmenting path 
     *  @return List of inserted orphans with label current_max_
     */
    std::vector<node_id> reduce_path(node_id leaf); 

    /** @brief  Increments the upper bound of the label of this subtree 
     */
    void increment_current_max();

    /** @brief  Adds a node to the subtree with label l as a child of pred 
     *  @param  Label of the node to be added
     *  @param  Node to be added
     *  @param  Predecessor of the node to be added
     */
    void add_node(label l, node_id node, node_id pred);

    /** @brief  Provides access to the nodes with the highest label
     *  @return Hash-Set of nodes with highest label
     */
    std::unordered_set<node_id>& get_front() {
        return labels_[current_max_];
    };


    label get_current_max() {
        return current_max_;
    };
    
    int get_id() const {
        return id_;
    };

    const std::vector<node_id>& get_last_resid_orphans() const {
        return last_resid_orphans_;
    };

    const std::vector<node_id>& get_last_flow() const {
        return last_flow_;
    };


private:
    int id_;
    const graph& g_;

    std::vector<std::unordered_set<node_id>> labels_;
    std::map<node_id,node_id> pred_;     
    std::map<node_id,std::unordered_set<node_id>> succ_;
    
    label current_max_ = 0;
    
    std::vector<node_id> last_flow_;
    std::vector<node_id> last_resid_orphans_;
    
    label adopt(node_id orphan, label min);
};

#endif /* IBFSSUBTREE_HPP_ */
