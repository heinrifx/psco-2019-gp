#ifndef INCREMENTALBFS_H_
#define INCREMENTALBFS_H_

#include <vector>
#include "ibfs_subtree.hpp"
#include "graph.hpp"


class incremental_bfs {
public:
    /** @brief  Initializes an incremental BFS instance
     *  @param  Graph to be partitioned
     *  @param  List of sources for S-T-cut
     *  @param  List of sinks for S-T-cut
     */
    incremental_bfs(const graph& g, std::vector<node_id> sources, std::vector<node_id> sinks);

    /** @brief  Executes the IBFS algorithm
     */
    void run();

    /** @brief  Returns the flow value after execution, which corresponds to the minimal S-T-cut
     */
    int get_max_flow() const {
        return flow_;
    };

private:
    enum subtree { s = -1, t = 1, none, flow, s_root, t_root };
   
    const graph& g_;
    
    std::vector<node_id> sources_;
    std::vector<node_id> sinks_;

    ibfs_subtree s_;
    ibfs_subtree t_;

    std::vector<subtree> node_assignments_;
    
    unsigned int flow_ = 0;

    bool grow(ibfs_subtree& st);
};

#endif /* INCREMENTALBFS_H_ */
