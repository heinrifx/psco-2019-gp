#include <catch2/catch.hpp>

#include <gp-bnb/graph.hpp>
#include <gp-bnb/metis_reader.hpp>
#include <gp-bnb/bnb.hpp>

// Tests for Branch&Bound algorithm

TEST_CASE("BnbTest") {


    //test partitioning of tiny graph
    std::string graph = "../test/inputs/tiny_01.graph";
    auto g = metis_reader().read(graph);
    auto sol = gp_bnb::solver(g, gp_bnb::lb::none);
    sol.solve();
    std::vector<partition::subgraph> f= sol.best_solution();
    //node 1, 2,3 and 5 in one partition
    REQUIRE(f[0] == f[1]);
    REQUIRE(f[1] == f[2]);
    REQUIRE(f[2] == f[4]);
    //node 4, 6, 7 in the other one
    REQUIRE(f[3] == f[5]);
    REQUIRE(f[5] == f[6]);


    //test solver
    //
    

    //test expand-function

    //test backtrack-function

    //test next_possible_subgraph


    //tests partition functions
    //
    node_id node;
    node = 2;
    partition::subgraph sg;
    sg = partition::subgraph::sg_a;
    partition par(g);

    REQUIRE(par.num_nodes_of(partition::sg_a) == 0);
    REQUIRE(par.num_nodes_of(partition::sg_b) == 0);
    REQUIRE(par.num_nodes_of(partition::none) == 7);
    REQUIRE(par.assigned_subgraph_of(node) == partition::subgraph::none); 

    //test partition::assign_node
    par.assign_node(node, sg);
    REQUIRE(par.assigned_subgraph_of(node) == partition::subgraph::sg_a);
    REQUIRE(par.num_nodes_of(partition::sg_a) == 1);
    REQUIRE(par.num_nodes_of(partition::none) == 6);
    REQUIRE(par.num_nodes_of(partition::sg_b) == 0);

    node = 4;
    sg = partition::subgraph::sg_b;
    par.assign_node(node, sg);
    REQUIRE(par.assigned_subgraph_of(node) == partition::subgraph::sg_b);
    REQUIRE(par.num_nodes_of(partition::sg_a) == 1);
    REQUIRE(par.num_nodes_of(partition::none) == 5);
    REQUIRE(par.num_nodes_of(partition::sg_b) == 1);
    
    //REQUIRE_THROWS(par.assign_node(node, partition::subgraph::none));
    //REQUIRE_THROWS(par.unassign_node(3));
    
    //test partition::unassign_node
    node = 2;
    par.unassign_node(node);
    REQUIRE(par.assigned_subgraph_of(node) == partition::subgraph::none);
    REQUIRE(par.num_nodes_of(partition::sg_a) == 0);
    REQUIRE(par.num_nodes_of(partition::none) == 6);
    REQUIRE(par.num_nodes_of(partition::sg_b) == 1);

    node = 4;
    par.unassign_node(node);
    REQUIRE(par.assigned_subgraph_of(node) == partition::subgraph::none);
    REQUIRE(par.num_nodes_of(partition::sg_a) == 0);
    REQUIRE(par.num_nodes_of(partition::none) == 7);
    REQUIRE(par.num_nodes_of(partition::sg_b) == 0);


    //trail_state
    //
    auto tr = gp_bnb::trail_state();
    node = 2;
    REQUIRE(tr.length() == 0);
    
    //test trail_state::push_node
    tr.push_node(node, partition::subgraph::sg_a);
    REQUIRE(tr.length() == 1);
    REQUIRE(tr.node_at(0) == 2);
    REQUIRE(tr.alternative_at(0) == partition::sg_a);
    node = 3;
    tr.push_node(node, partition::subgraph::sg_b);
    REQUIRE(tr.length() == 2);
    REQUIRE(tr.node_at(1) == 3);
    REQUIRE(tr.alternative_at(1) == partition::sg_b);

    //test trail_state::pop
    tr.pop();
    REQUIRE(tr.length() == 1);
    REQUIRE(tr.node_at(0) == 2);
    REQUIRE(tr.alternative_at(0) == partition::sg_a);
    tr.pop();
    REQUIRE(tr.length() == 0);
    


}
