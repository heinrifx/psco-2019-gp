#define CATCH_CONFIG_MAIN // It tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include <gp-bnb/graph.hpp>

// Tests for Graph data structure

// Example 1.
TEST_CASE("GraphStructureTest") {

    // Petersen Graph
    std::vector<std::vector<unsigned int>> adjacencies {
        {2, 3, 4}, {1, 3, 7},
        {2, 4, 8}, {3, 5, 9},
        {1, 4, 10}, {1, 8, 9},
        {2, 9, 10}, {3, 6, 10},
        {4, 6, 7}, {5, 7, 8}
    };
    
    graph g = graph(adjacencies);

    REQUIRE(g.num_nodes() == 10);
}