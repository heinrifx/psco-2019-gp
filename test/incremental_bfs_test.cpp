#include <vector>

#include <catch2/catch.hpp>

#include <gp-bnb/incremental_bfs.hpp>
#include <gp-bnb/graph.hpp>
#include <gp-bnb/metis_reader.hpp>

// Tests for Incremental BFS Max Flow algorithm

TEST_CASE("IncrementalBFSMaxFlow") {
    
    std::vector<std::vector<node_id>> adjacency_list {
        { 2, 7 },           // 1
        { 1, 3, 4 },        // 2
        { 2, 5 },           // 3
        { 2, 5 },           // 4
        { 3, 4, 6},         // 5
        { 5, 8 },           // 6
        { 1, 8 },           // 7
        { 6, 7 }            // 8
    };

    graph g (adjacency_list) ;

    std::vector<node_id> sources { 1 };
    std::vector<node_id> sinks { 8 };
    incremental_bfs ibfs = incremental_bfs(g, sources, sinks);

    ibfs.run();
    int max_flow = ibfs.get_max_flow();

    REQUIRE(max_flow == 2);

    std::string graph_file = "../test/inputs/tiny_01.graph";
    graph g2 = metis_reader().read(graph_file);
    
    std::vector<node_id> sources2 { 1 };
    std::vector<node_id> sinks2 { 7 };

    incremental_bfs ibfs2 = incremental_bfs(g2, sources2, sinks2);

    ibfs2.run();
    int max_flow2 = ibfs2.get_max_flow();

    REQUIRE(max_flow2 == 2);

}
