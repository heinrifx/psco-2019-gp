#include <catch2/catch.hpp>

#include <gp-bnb/graph.hpp>
#include <gp-bnb/metis_reader.hpp>

// Tests for MetisReader

TEST_CASE("MetisReaderTest") {
    
    std::string graphFile = "../test/inputs/tiny_01.graph";
    graph g = metis_reader().read(graphFile);

    REQUIRE(g.num_nodes() == 7);
}